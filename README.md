# OpenML dataset: TurkiyeStudentEvaluation

https://www.openml.org/d/4553

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Abstract: This data set contains a total 5820 evaluation scores provided by students from Gazi University in Ankara (Turkey). There is a total of 28 course specific questions and additional 5 attributes.
Source:

Ernest Fokoue 
Center for Quality and Applied Statistics 
Rochester Institute of Technology 
98 Lomb Memorial Drive 
Rochester, NY 14623, USA 
eMa&Auml;&plusmn;l: epfeqa '@' rit.edu 

Necla Gunduz 
Department of Statistics 
Faculty of Science, Gazi University 
Teknikokullar,06500 Ankara, Turkey 
eMail: ngunduz '@' gazi.edu.tr 
gunduznecla '@' yahoo.com


Data Set Information:

N/A


Attribute Information:

instr: Instructor's identifier; values taken from {1,2,3} 
class: Course code (descriptor); values taken from {1-13} 
repeat: Number of times the student is taking this course; values taken from {0,1,2,3,...} 
attendance: Code of the level of attendance; values from {0, 1, 2, 3, 4} 
difficulty: Level of difficulty of the course as perceived by the student; values taken from {1,2,3,4,5} 
Q1: The semester course content, teaching method and evaluation system were provided at the start. 
Q2: The course aims and objectives were clearly stated at the beginning of the period. 
Q3: The course was worth the amount of credit assigned to it. 
Q4: The course was taught according to the syllabus announced on the first day of class. 
Q5: The class discussions, homework assignments, applications and studies were satisfactory. 
Q6: The textbook and other courses resources were sufficient and up to date. 
Q7: The course allowed field work, applications, laboratory, discussion and other studies. 
Q8: The quizzes, assignments, projects and exams contributed to helping the learning. 
Q9: I greatly enjoyed the class and was eager to actively participate during the lectures. 
Q10: My initial expectations about the course were met at the end of the period or year. 
Q11: The course was relevant and beneficial to my professional development. 
Q12: The course helped me look at life and the world with a new perspective. 
Q13: The Instructor's knowledge was relevant and up to date. 
Q14: The Instructor came prepared for classes. 
Q15: The Instructor taught in accordance with the announced lesson plan. 
Q16: The Instructor was committed to the course and was understandable. 
Q17: The Instructor arrived on time for classes. 
Q18: The Instructor has a smooth and easy to follow delivery/speech. 
Q19: The Instructor made effective use of class hours. 
Q20: The Instructor explained the course and was eager to be helpful to students. 
Q21: The Instructor demonstrated a positive approach to students. 
Q22: The Instructor was open and respectful of the views of students about the course. 
Q23: The Instructor encouraged participation in the course. 
Q24: The Instructor gave relevant homework assignments/projects, and helped/guided students. 
Q25: The Instructor responded to questions about the course inside and outside of the course. 
Q26: The Instructor's evaluation system &amp;#40;midterm and final questions, projects, assignments, etc.&amp;#41; effectively measured the course objectives. 
Q27: The Instructor provided solutions to exams and discussed them with students. 
Q28: The Instructor treated all students in a right and objective manner. 

Q1-Q28 are all Likert-type, meaning that the values are taken from {1,2,3,4,5}


Relevant Papers:

N/A



Citation Request:

If you publish material based on databases obtained from this repository, then, in your acknowledgements, please note the assistance you received by using this repository. This will help others to obtain the same data sets and replicate your experiments. We suggest the following pseudo-APA reference format for referring to this repository: 

Gunduz, G. &amp; Fokoue, E. (2013). UCI Machine Learning Repository [[Web Link]]. Irvine, CA: University of California, School of Information and Computer Science. 

Here is a BiBTeX citation as well: 

@misc{GunduzFokoue:2013 , 
author = 'Gunduz, N. and Fokoue, E.', 
year = '2013', 
title = '{UCI} Machine Learning Repository', 
url = '[Web Link]', 
institution = 'University of California, Irvine, School of Information and Computer Sciences' }

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4553) of an [OpenML dataset](https://www.openml.org/d/4553). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4553/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4553/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4553/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

